private Long getConfluencePageId(remoteIssueLinks)
{
    for(link in remoteIssueLinks)
    {
        if(link.title.toString() == "Wiki Page")
        {
            def url = link.url.toString()
            confluencePageId = Long.parseLong(url.substring(url.indexOf("=")+1).replace("]",""))

            return confluencePageId
        }
        else
        {
            log.error "Link title: ${link.title.toString()}"
        }
    }
}