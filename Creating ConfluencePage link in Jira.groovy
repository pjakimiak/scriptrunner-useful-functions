
/*
confluenceLink = link to primary Confluence instance linked with Jira instance. Variable of type Application Link
        Example how to get you can find in "Getting primary Confluence link"
* */
private linkNewConfluencePage(confluenceLink, pageTitle, mainIssue, currentPageId)
{
    def linkBuilder = new RemoteIssueLinkBuilder()
    pageTitleUrl = pageTitle.replace(" ","+");

    linkBuilder.issueId(mainIssue.getId())
    linkBuilder.applicationName(confluenceLink.getName().toString())
    linkBuilder.applicationType("com.atlassian.confluence")
    linkBuilder.globalId("appId=bdb2122b-6c86-3a28-8a35-5dee79812e5a&pageId=${currentPageId}")
    linkBuilder.relationship("Wiki Page")
    linkBuilder.title("${pageTitle}")

    linkBuilder.url("${confluenceLink.getDisplayUrl()}/display/AN/${pageTitleUrl}")

    log.error linkBuilder.url
    def link = linkBuilder.build()
    def remoteIssueLinkService = ComponentManager.getComponentInstanceOfType(RemoteIssueLinkService.class)
    def validationResult = remoteIssueLinkService.validateCreate(getUser(), link)
    def createResult = remoteIssueLinkService.create(getUser(), validationResult);
    RemoteIssueLinkService.RemoteIssueLinkListResult links = remoteIssueLinkService.getRemoteIssueLinksForIssue(getUser(), mainIssue)
}