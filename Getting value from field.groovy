private getValueFromField(MutableIssue issue, String fieldName)
{
    def customFieldManager = ComponentAccessor.getCustomFieldManager()
    def fieldObject = customFieldManager.getCustomFieldObjectsByName(fieldName)
    def fieldValue = issue.getCustomFieldValue(fieldObject)

    return fieldValue
}